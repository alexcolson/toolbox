/**
 * @file    ###_FILENAME_H_###
 * @author  Sebastien Legrand
 * @email   oaxley@gmail.com
 * @date    ###_DATE_###
 *
 * @brief   Interface / here come the description
 */

#ifndef ###_GUARD_###
#define ###_GUARD_###

// standard includes

// personal includes


// class definition
class ###_OBJECT_###
{
###_CLASS_CONTENT_###
};

#endif // ###_GUARD_###

