/**
 * @file    ###_FILENAME_CPP_###
 * @author  Sebastien Legrand
 * @email   oaxley@gmail.com
 * @date    ###_DATE_###
 *
 * @brief   Implementation / here come the description
 */

// standard includes

// personal includes
#include "###_FILENAME_H_###"

//{ class implementation

###_CLASS_CONTENT_###

//}

