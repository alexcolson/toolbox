#!/bin/bash
# Syntax: generate.sh -object <object_name> [-no-source] [-output <path>]

#
# GLOBALS
#
PROGNAME=`basename $0`
OBJECT_NAME=""
NO_SOURCE="no"
OUTPUT_DIR="."
FILE=""
PROPERTY_FILE=""
PROPERTIES=( )

PRIVATE_MEMBERS="no"
PRIVATE_METHODS="no"
PUBLIC_MEMBERS="no"
PUBLIC_METHODS="no"


TMP_FILE=${PROGNAME}.$$

#
# FUNCTIONS
#
function _help {
    echo "Syntax: ${PROGNAME} [-file <path/file>] [-property <path/file>] [-object <name>] [-header] [-output <path>]"
    echo "-object    : specify the class name"
    echo "-header    : do not generate a .cpp file"
    echo "-output    : output directory for the generated files"
    echo "-property  : property file to create"
    echo "-file      : object description file [not implemented]"
}

# read the property file
function _readProperty {
    PATTERN='^#|^$'
    while read LINE
    do
        [[ ${LINE} =~ ${PATTERN} ]] && continue
        PROPERTIES[${#PROPERTIES[@]}]=${LINE}
    done < ${PROPERTY_FILE}
}

# create members
function _createMembers {
    index=0
    max_items=${#PROPERTIES[@]}
    while [[ $index -lt $max_items ]];
    do
        NAME=`echo ${PROPERTIES[$index]} | cut -d\; -f1`
        TYPE=`echo ${PROPERTIES[$index]} | cut -d\; -f2`
        SCOPE=`echo ${PROPERTIES[$index]} | cut -d\; -f3 | tr "[A-Z]" "[a-z]"`
        COMMENT=`echo ${PROPERTIES[$index]} | cut -d\; -f5`

        if [ "$SCOPE" == "private" ]; then
            printf "        %-5s m_%-20s; // %s\n" ${TYPE} ${NAME} "${COMMENT}" >> ${TMP_FILE}.private_members 
            PRIVATE_MEMBERS="yes"
        else
            printf "        %-5s m_%-20s; // %s\n" ${TYPE} ${NAME} "${COMMENT}" >> ${TMP_FILE}.public_members 
            PUBLIC_MEMBERS="yes"
        fi
        
        ((index++))
    done
}

function _createCDTor {
    PUBLIC_METHODS="yes"

    if [ "$NO_SOURCE" == "yes" ]; then
        # header only
        echo -ne "        // constructor\n        ${OBJECT_NAME}" >> ${TMP_FILE}.public_methods

        if [ "$PROPERTY_FILE" != "" ]; then
            echo -ne " :\n            "                   >> ${TMP_FILE}.public_methods

            # generate the properties initialization
            first=1
            index=0
            max_items=${#PROPERTIES[@]}
            while [[ $index -lt $max_items ]];
            do
                NAME=`echo ${PROPERTIES[$index]} | cut -d\; -f1`
                SCOPE=`echo ${PROPERTIES[$index]} | cut -d\; -f3 | tr "[A-Z]" "[a-z]"`
                if [ "$SCOPE" == "private" ]; then
                    if [[ $first -eq 0 ]]; then
                        echo -n ", " >> ${TMP_FILE}.public_methods
                    fi
                    echo -n "m_${NAME}(0)" >> ${TMP_FILE}.public_methods
                    first=0
                fi

                ((index++))
            done
        fi
        echo -e " {"          >> ${TMP_FILE}.public_methods
        echo -e "        }\n" >> ${TMP_FILE}.public_methods
        
        # generate the destructor
        echo "        // destructor"                >> ${TMP_FILE}.public_methods
        echo "        virtual ~${OBJECT_NAME}( ) {" >> ${TMP_FILE}.public_methods
        echo -e "        }\n"                            >> ${TMP_FILE}.public_methods
    else
        # header + source
        echo "        ${OBJECT_NAME}( );                         ///< constructor" >> ${TMP_FILE}.public_methods
        echo "        virtual ~${OBJECT_NAME}( );                ///< destructor"  >> ${TMP_FILE}.public_methods
        echo "" >> ${TMP_FILE}.public_methods

        # create the constructor
        echo -ne "// constructor\n ${OBJECT_NAME}::${OBJECT_NAME}( )" >> ${TMP_FILE}.xtor_source
        
        # generate the properties initialization
        if [ "$PROPERTY_FILE" != "" ]; then
            echo -ne " :\n    " >> ${TMP_FILE}.xtor_source
            first=1
            index=0
            max_items=${#PROPERTIES[@]}
            while [[ $index -lt $max_items ]];
            do
                NAME=`echo ${PROPERTIES[$index]} | cut -d\; -f1`
                SCOPE=`echo ${PROPERTIES[$index]} | cut -d\; -f3 | tr "[A-Z]" "[a-z]"`
                if [ "$SCOPE" == "private" ]; then
                    if [[ $first -eq 0 ]]; then
                        echo -n ", "       >> ${TMP_FILE}.xtor_source
                    fi
                    echo -n "m_${NAME}(0)" >> ${TMP_FILE}.xtor_source
                    first=0
                fi
                ((index++))
            done
        fi
        echo -e "\n{\n}\n" >> ${TMP_FILE}.xtor_source

        # create the destructor
        echo "// destructor"                                  >> ${TMP_FILE}.xtor_source
        echo "/*virtual*/ ${OBJECT_NAME}::~${OBJECT_NAME}( )" >> ${TMP_FILE}.xtor_source
        echo -e "{\n}\n"                                      >> ${TMP_FILE}.xtor_source
    fi
}

function _createProperties {
    index=0
    max_items=${#PROPERTIES[@]}
    while [[ $index -lt $max_items ]];
    do
        NAME=`echo ${PROPERTIES[$index]} | cut -d\; -f1`
        TYPE=`echo ${PROPERTIES[$index]} | cut -d\; -f2`
        SCOPE=`echo ${PROPERTIES[$index]} | cut -d\; -f3`
        ACCESS=`echo ${PROPERTIES[$index]} | cut -d\; -f4 | tr "[A-Z]" "[a-z]"`

        OUTFILE=${TMP_FILE}.public_methods
        PUBLIC_METHODS="yes"

        if [ "$NO_SOURCE" == "yes" ]; then
            # header only
            
            echo "        //{ ${NAME} get/set ($ACCESS)"       >> ${OUTFILE}
            echo "        ${TYPE} ${NAME}( ) const {"          >> ${OUTFILE}
            echo "            return m_${NAME};"               >> ${OUTFILE}
            echo "        }"                                   >> ${OUTFILE}

            # generate the setter in a readwrite mode
            if [ "$ACCESS" == "readwrite" ]; then
                echo ""                                         >> ${OUTFILE}
                echo "        void set${NAME}(${TYPE} value) {" >> ${OUTFILE}
                echo "            m_${NAME} = value;"           >> ${OUTFILE}
                echo "        }"                                >> ${OUTFILE}
            fi
            echo -e "        //}\n" >> ${OUTFILE}
        else
            # header + source
            
            # push the prototypes in the header
            echo "        //{ ${NAME} get/set ($ACCESS)" >> ${OUTFILE}
            echo "        ${TYPE} ${NAME}( ) const;"     >> ${OUTFILE}
            
            # generate the setter in a readwrite mode
            if [ "$ACCESS" == "readwrite" ]; then
                echo "        void set${NAME}(${TYPE} value);" >> ${OUTFILE}
            fi
            echo -e "        //}\n" >> ${OUTFILE}

            # create the function in the source
            echo -e "\n//{ ${NAME} get/set  (${ACCESS})"       >> ${TMP_FILE}.property_source
            echo    "${TYPE} ${OBJECT_NAME}::${NAME}( ) const" >> ${TMP_FILE}.property_source
            echo    "{"                                        >> ${TMP_FILE}.property_source
            echo    "    return m_${NAME};"                    >> ${TMP_FILE}.property_source
            echo -e "}"                                        >> ${TMP_FILE}.property_source

            # create the setter in readwrite mode
            if [ "$ACCESS" == "readwrite" ]; then
                echo -e "\nvoid ${OBJECT_NAME}::set${NAME}(${TYPE} value)" >> ${TMP_FILE}.property_source
                echo -e "{\n    m_${NAME} = value;\n}"                   >> ${TMP_FILE}.property_source                                              
            fi
            echo    "//}" >> ${TMP_FILE}.property_source
        fi

        ((index++))
    done
}

#
# BEGIN
#

# read the cmdline arguments
while [ $# -gt 0 ]
do
    case "$1" in
        -object)
            shift
            OBJECT_NAME=$1
            shift
            ;;
        -header)
            NO_SOURCE="yes"
            shift
            ;;
        -output)
            shift
            OUTPUT_DIR=$1
            shift
            ;;
        -property)
            shift
            PROPERTY_FILE=$1
            shift
            ;;
        -file)
            shift
            FILE=$1
            shift
            ;;
        *)
            shift
            ;;
    esac
done

if [ "$OBJECT_NAME" == "" ]; then
  _help
  exit
fi

# create varnames for the sed
FILENAME=`echo $OBJECT_NAME | tr "[A-Z]" "[a-z]"`
DATE=`date "+%Y-%m-%d"`
GUARD=`echo "${OBJECT_NAME}_H" | tr "[a-z]" "[A-Z]"`

cat << EOF > ${TMP_FILE}.sed
s!###_FILENAME_H_###!${FILENAME}.h!
s!###_FILENAME_CPP_###!${FILENAME}.cpp!
s!###_DATE_###!${DATE}!
s!###_OBJECT_###!${OBJECT_NAME}!
s!###_GUARD_###!${GUARD}!
EOF

# create the member/method files
echo -e "    private:            // private members\n" > ${TMP_FILE}.private_members
echo -e "    public:             // public members\n" > ${TMP_FILE}.public_members
echo -e "    private:            // private methods\n" > ${TMP_FILE}.private_methods
echo -e "    public:             // public methods\n" > ${TMP_FILE}.public_methods

# read properties if any
if [ "${PROPERTY_FILE}" != "" ]; then
    _readProperty
    _createMembers
fi

# create constructor / destructor
_createCDTor

# create properties setters/getters
if [ "${PROPERTY_FILE}" != "" ]; then
    _createProperties
fi

# create the heade 
    
# generate the temporary file
cat blank.h | sed -f ${TMP_FILE}.sed > ${TMP_FILE}.h

# compose the class content
cat /dev/null > ${TMP_FILE}.class_content
if [ "$PRIVATE_MEMBERS" == "yes" ]; then
    cat ${TMP_FILE}.private_members >> ${TMP_FILE}.class_content
    echo "" >> ${TMP_FILE}.class_content
fi
if [ "$PRIVATE_METHODS" == "yes" ]; then
    cat ${TMP_FILE}.private_methods >> ${TMP_FILE}.class_content
    echo "" >> ${TMP_FILE}.class_content
fi
if [ "$PUBLIC_MEMBERS" == "yes" ]; then
    cat ${TMP_FILE}.public_members >> ${TMP_FILE}.class_content
    echo "" >> ${TMP_FILE}.class_content
fi
if [ "$PUBLIC_METHODS" == "yes" ]; then
    cat ${TMP_FILE}.public_methods >> ${TMP_FILE}.class_content
    echo "" >> ${TMP_FILE}.class_content
fi

# replace the ###_CLASS_CONTENT_###
sed '
/###_CLASS_CONTENT_###/ {
r '${TMP_FILE}.class_content'
d
}' < ${TMP_FILE}.h > ${FILENAME}.h
    

# create the source if needed
if [ "$NO_SOURCE" == "no" ]; then
    cat blank.cpp | sed -f ${TMP_FILE}.sed > ${TMP_FILE}.cpp

    # prepare the class content
    cat ${TMP_FILE}.xtor_source > ${TMP_FILE}.class_content
    if [ -f ${TMP_FILE}.property_source ]; then
        cat ${TMP_FILE}.property_source >> ${TMP_FILE}.class_content
    fi

    # replace the ###_CLASS_CONTENT_###
    sed '
    /###_CLASS_CONTENT_###/ {
    r '${TMP_FILE}.class_content'
    d
    }' < ${TMP_FILE}.cpp > ${FILENAME}.cpp
fi

# end
rm -f ${TMP_FILE}*
echo "Done."
