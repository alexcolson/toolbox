#!/usr/bin/python

"""
    @file       parrot.py
    @author     Sebastien Legrand (oaxley)
    @email      oaxley@gmail.com
    @date       2011-10-25
    @brief      Parrot send you back all string you sent to him. He appends '[]' around the string
"""


#---------------
# libraries
#---------------
import io
import sys
import socket
from optparse import OptionParser

#---------------
# globals
#---------------
BUFFSIZE = 4096

port = 0
sock = None

#---------------
# begin
#---------------

# parse the command line
parser = OptionParser(usage="%prog [--address=<address>] [--udp] [--prefix=<string>] <port>", version="1.0")
parser.add_option("", "--address", dest="host", help="bind to a specific address", default="", metavar="address")
parser.add_option("", "--prefix", dest="prefix", help="add a prefix string to the return string", default="", metavar="string")
parser.add_option("", "--udp", dest="isUDP", action="store_true", help="select UDP as the transport protocol (default:TCP)", default=False)
(options,args) = parser.parse_args()

# retrieve the port number
if len(args) < 1:
    sys.stderr.write(sys.argv[0] + ' [-h|--help] [--address=<address>] [--udp] <port>' + '\n')
    sys.exit(1)

# convert the port to an integer
try:
    port=int(args[0])
except ValueError:
    sys.stderr.write('Error: TCP port should be an integer between [0..65535] !')
    sys.exit(2)

# create the server socket
try:
    if options.isUDP:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind( (options.host,port) )
    else:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind( (options.host,port) )
        sock.listen(1)
except socket.error, (value,message):
    sys.stderr.write('Error while initializing socket layer:' + message + '\n')
    sys.exit(1)

# wait for new connection
if options.isUDP:
    print 'Parrot is running on port %d via UDP ... (prefix=<%s>)' % (port,options.prefix)
else:
    print 'Parrot is running on port %d via TCP ... (prefix=<%s>)' % (port,options.prefix)

try:
    while 1:
        if options.isUDP:
            data,addr = sock.recvfrom(BUFFSIZE)
            print 'Data received from',addr
            
            # resend the same data
            string=options.prefix+'['+data.rstrip('\n\r')+']\n'
            sock.sendto(string, addr)
        else:
            cnx,addr = sock.accept()
            print 'New connection from',addr

            # read input data
            while 1:
                data = cnx.recv(BUFFSIZE)
                if not data: 
                    break
            
                # resend the same data without the end-line 
                string=options.prefix+'['+data.rstrip('\n\r')+']\n'
                cnx.send(string)
            cnx.close()

except KeyboardInterrupt:
    sock.close()
