/**
 * @file    HexDump.cpp
 * @author  Sebastien Legrand (oaxley)
 * @email   oaxley@gmail.com
 * @date    2009-11-23
 *
 * @brief   Implement a basic hexadecimal dump in C++
 */

// standard includes
#include <iostream>
#include <fstream>
#include <string>

// C includes
#include <cstring>
#include <cstdlib>

// main entry point
int main(int argc, char *argv[])
{
    std::ifstream infile;
    int nMaxSize = 0;
    bool bDone = false;

    // no arguments given
    if( 1 == argc ) 
    {
        std::cout << "HexDump <filename> [size]" << std::endl;
        return 0;
    }

    // 2 arguments given
    if( 3 == argc )
        nMaxSize = atoi(argv[2]);

    infile.open(argv[1], std::ifstream::binary|std::ifstream::in );
    if( infile.good() && (!bDone) ) 
    {
        char szBuffer[256] = {0};
        int nCounter = 0;

        // header
        std::cout << std::endl;
        std::cout << "         00|01|02|03|04|05|06|07|08|09|0A|0B|0C|0D|0E|0F" << std::endl;

        // read the file 16bytes at a time
        int nRemain = 16;
        while( (infile.good()) && (!bDone) ) 
        {
            // read 16 chars at a time
            infile.read(szBuffer,nRemain);

            std::string strText(16,'.');
            std::string strHexa="";
            char szBuffer2[10]={0};

            if( infile.gcount() < nRemain )
                nRemain = infile.gcount();

            int i;
            for(i=0;i<nRemain;i++)
            {
                if( ((unsigned char)szBuffer[i] > 31) && ((unsigned char)szBuffer[i] < 127) )
                    strText[i]=szBuffer[i];

                sprintf(szBuffer2,"%02X ",(unsigned char)szBuffer[i]);
                strHexa+=std::string(szBuffer2);
            }

            // add more space in case we reach the EOF prematurely
            for(;i<16;i++) 
            {
                strText[i] = ' ';
                strHexa += std::string("   ");
            }

            // print the result in STDOUT
            std::cout.setf(std::ios::hex, std::ios::basefield);
            std::cout.width(8);
            std::cout.fill('0');
            std::cout << nCounter;
            std::cout.unsetf(std::ios::hex);
            std::cout << " " << strHexa << strText << std::endl;

            // empty the buffer
            memset(szBuffer,0,20);

            // increase counter and check if we are done
            nCounter += nRemain;
            if( nMaxSize > 0 )
            {
                if( nCounter >= nMaxSize )
                    bDone = true;
                else
                {
                    nRemain = nMaxSize - nCounter;
                    if( nRemain > 16 )
                        nRemain = 16;
                }
            }
        }
    } else {
        std::cerr << "Unable to open the file [" << argv[1] << "] for reading !!!" << std::endl;
    }

    // footer
    std::cout << "         00|01|02|03|04|05|06|07|08|09|0A|0B|0C|0D|0E|0F" << std::endl;
    std::cout << std::endl;

    return 0;
}
