#!/usr/bin/python

#----------------
# globals
#----------------

# buffer size for socket operations
BUFFSIZE = 4096

# Timeout
TIMEOUT = 0.5

# admin messages
ADM_QUIT_PROGRAM = 0x00FF
ADM_ADD_SOCKET = 0x0001
ADM_DEL_SOCKET = 0x0002
ADM_DISCONNECT = 0x0003


THREAD_CLOSE= 0x0010

# IN messages
IN_ENTER = 0x0101
IN_LEAVE = 0x0102
PUBLISH_IN = 0x0104

# OUT messages
OUT_ENTER = 0x0201
OUT_LEAVE = 0x0202
PUBLISH_OUT = 0x0204

# history file location
HISTORY_FILE = ".proxy.history"
