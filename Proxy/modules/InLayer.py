#!/usr/bin/python

"""
    Upper Layer in charge of incoming connection
"""

#--------------------
# libraries
#--------------------
import io
import sys
import socket
import select

import EventLoop
import Settings

#--------------------
# globals
#--------------------

#--------------------
# class definition
#--------------------
class InLayer:

    def __init__(self, hostname, port, protocol, eventloop):
        """ constructor """
        self.hostname = hostname
        self.port = port
        self.protocol = protocol
        self.eventloop = eventloop
        self.socket = None
        self.in_socket = None
        self.isServer = False

    def getSocket(self):
        """ return the object socket """
        return self.socket

    def createServer(self):
        """ create the TCP or UDP server depending on the protocol """
        self.isServer = True
        if self.protocol == 'TCP':
            self.tcpServer()
        else:
            self.udpServer()

    def tcpServer(self):
        """ create a TCP server """
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind( (self.hostname,self.port) )
        self.socket.listen(1)
        self.isServer = True

    def udpServer(self):
        """ create an UDP server """
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind( (self.hostname,self.port) )

    def onSelect(self,sock):
        """ the object has been selected by the select method """
        if self.protocol == 'TCP':
            self.onSelectTCP(sock)
        else:
            self.onSelectUDP(sock)

    def onSelectTCP(self,sock):
        """ select TCP """
        if self.isServer and (sock == self.socket):
            (cnx,addr) = sock.accept()
            self.in_socket = cnx
            self.eventloop.postMessage((Settings.ADM_ADD_SOCKET,(self,cnx)))
        else:
            data = sock.recv(Settings.BUFFSIZE)
            if not data:
                self.eventloop.postMessage((Settings.ADM_DEL_SOCKET,(self,sock)))
                self.in_socket = None
            else:
                self.eventloop.postMessage((Settings.PUBLISH_OUT,data))

    def onSelectUDP(self,sock):
        """ select UDP """
        (data, self.in_socket) = sock.recvfrom(Settings.BUFFSIZE)
        self.eventloop.postMessage((Settings.PUBLISH_OUT,data))

    def setHandlers(self):
        """ attach handlers to events """
        self.eventloop.attachHandler(Settings.ADM_QUIT_PROGRAM,self.onQuit)
        self.eventloop.attachHandler(Settings.ADM_DISCONNECT,self.onDisconnect)
        self.eventloop.attachHandler(Settings.PUBLISH_IN,self.onPublishIn)

    def onQuit(self,data):
        """ ADM_QUIT_PROGRAM handler """
        self.socket.close()

    def onDisconnect(self,data):
        """ ADM_DISCONNECT handler """
        self.in_socket.close()
        self.socket.close()
        self.eventloop.postMessage((Settings.ADM_DEL_SOCKET,(self,self.in_socket)))
        self.eventloop.postMessage((Settings.ADM_DEL_SOCKET,(self,self.socket)))
        self.in_socket = None

    def onPublishIn(self,data):
        """ PUBLISH_IN handler: publish a message on the IN socket """
        if self.protocol == 'TCP':
            self.in_socket.send(data)
        else:
            self.socket.sendto(data,self.in_socket)

