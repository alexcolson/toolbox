#!/usr/bin/python

"""
    Lower Layer in charge of outgoing connection
"""

#--------------------
# libraries
#--------------------
import io
import sys
import socket
import select

import EventLoop
import Settings

#--------------------
# globals
#--------------------

#--------------------
# class definition
#--------------------
class OutLayer:

    def __init__(self, hostname, port, protocol, eventloop):
        """ constructor """
        self.hostname = hostname
        self.port = port
        self.protocol = protocol
        self.eventloop = eventloop
        self.socket = None
        self.in_socket = None

    def getSocket(self):
        """ return the object socket """
        return self.socket

    def createConnect(self):
        """ create the TCP or UDP connection depending on the protocol """
        if self.protocol == 'TCP':
            self.tcpConnect()
        else:
            self.udpConnect()

    def tcpConnect(self):
        """ create the TCP connection """
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.connect( (self.hostname,self.port) )
        except socket.error, (errno,message):
            sys.stderr.write('TCP: Unable to connect!\n')
            sys.exit(1)

        self.eventloop.postMessage((Settings.ADM_ADD_SOCKET,(self,self.socket)))

    def udpConnect(self):
        """ create the UDP connection """
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        except socket.error, (errno,message):
            sys.stderr.write('UDP: Unable to create socket!\n')
            sys.exit(1)

        self.eventloop.postMessage((Settings.ADM_ADD_SOCKET,(self,self.socket)))

    def onSelect(self,sock):
        """ the object has been selected by the select method """
        if self.protocol == 'TCP':
            self.onSelectTCP(sock)
        else:
            self.onSelectUDP(sock)

    def onSelectTCP(self,sock):
        """ select TCP """
        data = sock.recv(Settings.BUFFSIZE)
        if not data:
            self.eventloop.postMessage((Settings.ADM_DEL_SOCKET,(self,sock)))
            self.in_socket = None
        else:
            self.eventloop.postMessage((Settings.PUBLISH_IN,data))

    def onSelectUDP(self,sock):
        """ select UDP """
        (data, self.in_socket) = sock.recvfrom(Settings.BUFFSIZE)
        self.eventloop.postMessage((Settings.PUBLISH_IN,data))

    def setHandlers(self):
        """ attach handlers to events """
        self.eventloop.attachHandler(Settings.ADM_QUIT_PROGRAM,self.onQuit)
        self.eventloop.attachHandler(Settings.PUBLISH_OUT,self.onPublishOut)

    def onQuit(self,data):
        """ ADM_QUIT_PROGRAM handler """
        self.socket.close()

    def onPublishOut(self,data):
        """ PUBLISH_OUT handler: publish a message on the OUT socket """
        if self.protocol == 'TCP':
            self.socket.send(data)
        else:
            self.socket.sendto(data,(self.hostname,self.port))

