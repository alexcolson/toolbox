#!/usr/bin/env python

"""
    This class provides an Event API
    handlers should be form as handler(data)
"""

# Import
#-----


# Class definition
#-----
class EventLoop:

    def __init__(self):
        """ EventLoop constructor """
        self.event_table = {}    # dictionary to map and event ID to its handlers
        self.queue = []          # event queue

    def isEmpty(self):
        """ true is the event queue is empty """
        if len(self.queue) == 0:
            return True
        else:
            return False

    def attachHandler(self, event, handler):
        """ attach a handler to an event """
        if self.event_table.has_key(event):
            self.event_table[event].append(handler)
        else:
            self.event_table[event] = [handler]

    def detachHandler(self, event, handler):
        """ detach a handler from an event """
        if self.event_table.has_key(event): 
            self.event_table[event].remove(handler)

    def postMessage(self, (event,data)):
        """ post a message to the queue """
        self.queue.append((event,data)) 

    def getMessage(self):
        """ get a message from the queue """
        return self.queue.pop(0)

    def dispatchMessage(self, (event,data)):
        """ dispatch a message to all registered handlers """
        if self.event_table.has_key(event):
            for handler in self.event_table[event]:
                handler(data)



