#!/usr/bin/env python

#-------------------
# libraries
#-------------------
import re
import sys

import ConfigParser
import logging

from optparse import OptionParser
from modules import Settings
from modules import EventLoop
from modules import ProxyServer

LOG_FILENAME = 'simpleproxy.log'

logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG,format='%(asctime)-15s [%(levelname)s] (%(threadName)-10s) %(message)s',
                    )
logging.info('Starting Proxy')

#-------------------
# globals
#-------------------
isReadlineEnabled = True
try:
    import readline
except:
    print "Readline not available. Features will be disabled"
    isReadlineEnabled = False


# event loop to treat messages
adminLoop = EventLoop.EventLoop()
running = False

# variables memory
memory = { }


servers = []

#-------------------
# functions
#-------------------

def onQuitProgram(data):
    """ ADM_QUIT_PROGRAM handler """
    global running
    running = False
    sys.stderr.write('QUIT message received...\n')


def onPublishOut(data):
    global memory
    if memory['log_output'] and memory['output_file'] != '':
        f = open(memory['output_file'],'a')
        f.write(data);
        f.close()

def onPublishIn(data):
    global memory
    if memory['log_input'] and memory['input_file'] != '':
        f = open(memory['input_file'],'a')
        f.write(data);
        f.close()



#-------------------
# begin
#-------------------

# read cmdline parameters
parser = OptionParser(usage="%prog [--debug] [--config=<configuration file>]", version="1.0")
parser.add_option("", "--debug", dest="debug", action="store_true", help="set the debug flag to TRUE (default:FALSE)", default=False)
parser.add_option("", "--config", dest="configfile", help="specify a custom configuration file ", metavar="proxy.conf")
parser.add_option("", "--latency", dest="latency", help="specify an artificial latency", type=float, default=0.0 , metavar=0.0)

(options,args) = parser.parse_args()

# read the configuration file
config = ConfigParser.RawConfigParser()
if options.configfile == "" or options.configfile == None:
    sys.stderr.write('Error!: config file is mandatory\n')
    parser.print_help()
    sys.exit(1)

try:
    config.readfp( open(options.configfile) )
except IOError, (value,message):
    sys.stderr.write('Unable to open the configuration file [' + options.configfile + ']: ' + message + '\n')
    sys.exit(1)

# handlers
adminLoop.attachHandler(Settings.ADM_QUIT_PROGRAM,onQuitProgram)
adminLoop.attachHandler(Settings.PUBLISH_OUT,onPublishOut)
adminLoop.attachHandler(Settings.PUBLISH_IN,onPublishIn)

# create variables memory
memory['output_file'] = ''
memory['input_file'] = ''
memory['log_output'] = False
memory['log_input'] = False
memory['isConnected'] = True

logging.debug(options)

# read line history
if isReadlineEnabled:
    try:
        readline.read_history_file(Settings.HISTORY_FILE)
    except IOError:
        pass


# start the servers 
for section in config.sections():
    server = ProxyServer.ProxyServer(config.get(section,'inhost'),config.getint(section,'inport'),
            config.get(section,'outhost'),config.getint(section,'outport'),adminLoop,
            options.latency)
    # add the newly created server to the list of servers 
    servers.append(server)
    server.start()



# process input from the user
running = True
isSocketUp = True
while running:
    try:
        # retrieve the input from the user
        input = raw_input("proxy> ")
        cmd = input.split(' ')

        # execute command
        if re.search('^quit$',cmd[0].lower()):
            adminLoop.postMessage((Settings.ADM_QUIT_PROGRAM,0))
            running = False

        if re.search('^down$',cmd[0].lower()) and memory['isConnected']:
            adminLoop.postMessage((Settings.ADM_DISCONNECT,0))
            memory['isConnected'] = False

        if re.search('^status$',cmd[0].lower()):
            print memory

        if re.search('^set$',cmd[0].lower()):
            try:
                memory[cmd[1].lower()] = eval(cmd[2],{},{})
            except:
                pass

    except KeyboardInterrupt:
        adminLoop.postMessage((Settings.ADM_QUIT_PROGRAM,0))

#wait for server to terminate
for server in servers:
    server.join()


# write the history
if isReadlineEnabled:
    readline.write_history_file(Settings.HISTORY_FILE)

print 'Proxy stopped'



