#!/usr/bin/python

#-------------------
# libraries
#-------------------
import re
import sys
import select
import time
import threading
import ConfigParser


from optparse import OptionParser
from modules import Settings
from modules import EventLoop
from modules import InLayer
from modules import OutLayer

#-------------------
# globals
#-------------------
isReadlineEnabled = True
try:
    import readline
except:
    print "Readline not available. Features will be disabled"
    isReadlineEnabled = False


# event loop to treat messages
eventloop = EventLoop.EventLoop()
running = False

# table for select
in_select = [ ]

# keep track of all objects with their socket as primary key
objects = { }

# variables memory
memory = { }

#-------------------
# functions
#-------------------

def onQuitProgram(data):
    """ ADM_QUIT_PROGRAM handler """
    global running
    running = False
    sys.stderr.write('QUIT message received...\n')

def onAddSocket(data):
    """ ADM_ADD_SOCKET handler: add a socket to the listener list """
    global in_select
    global objects
    (obj,sock) = data
    in_select.append(sock)
    objects[sock] = obj

def onDelSocket(data):
    """ ADM_DEL_SOCKET handler: remove a socket to the listener list """
    global in_select
    global objects
    (obj,sock) = data
    in_select.remove(sock)
    del objects[sock]

def mainloop():
    """ thread mainloop that handle the communication between sockets """
    global running
    global in_select
    global objects
    global eventloop
    while running:
        
        # socket selector
        (inbound,outbound,error) = select.select(in_select,[],[],Settings.TIMEOUT)

        # handle inbound connection
        for sock in inbound:
            objects[sock].onSelect(sock)

        # treat messages
        while not eventloop.isEmpty():
            eventloop.dispatchMessage( eventloop.getMessage() )


def onPublishOut(data):
    global memory
    if memory['log_output'] and memory['output_file'] != '':
        f = open(memory['output_file'],'a')
        f.write(data);
        f.close()

def onPublishIn(data):
    global memory
    if memory['log_input'] and memory['input_file'] != '':
        f = open(memory['input_file'],'a')
        f.write(data);
        f.close()


#-------------------
# begin
#-------------------

# read cmdline parameters
parser = OptionParser(usage="%prog [--debug] [--config=<configuration file>]", version="1.0")
parser.add_option("", "--debug", dest="debug", action="store_true", help="set the debug flag to TRUE (default:FALSE)", default=False)
parser.add_option("", "--config", dest="configfile", help="specify a custom configuration file (default:proxy.conf)", default="proxy.conf", metavar="proxy.conf")
(options,args) = parser.parse_args()

# read the configuration file
config = ConfigParser.RawConfigParser()
try:
    config.readfp( open(options.configfile) )
except IOError, (value,message):
    sys.stderr.write('Unable to open the configuration file [' + options.configfile + ']: ' + message + '\n')
    sys.exit(1)

# handlers
eventloop.attachHandler(Settings.ADM_QUIT_PROGRAM,onQuitProgram)
eventloop.attachHandler(Settings.ADM_ADD_SOCKET,onAddSocket)
eventloop.attachHandler(Settings.ADM_DEL_SOCKET,onDelSocket)
eventloop.attachHandler(Settings.PUBLISH_OUT,onPublishOut)
eventloop.attachHandler(Settings.PUBLISH_IN,onPublishIn)

# create the IN layer
inlayer = InLayer.InLayer( config.get('IN','host'), config.getint('IN','port'), config.get('IN','protocol'), eventloop )
inlayer.createServer()
inlayer.setHandlers()

# create the OUT layers
for section in config.sections():
    if re.search('OUT_[0-9]+',section):
        outlayer = OutLayer.OutLayer( config.get(section,'host'), config.getint(section,'port'), config.get(section,'protocol'), eventloop )
        outlayer.createConnect()
        outlayer.setHandlers()

# add the socket to the selector
in_select.append(inlayer.getSocket())

# memorize object
objects[inlayer.getSocket()] = inlayer

# create variables memory
memory['output_file'] = ''
memory['input_file'] = ''
memory['log_output'] = False
memory['log_input'] = False
memory['isConnected'] = True

# create mainloop thread
running = True
eventThread = threading.Thread(target = mainloop)
eventThread.setDaemon(True)
eventThread.start()

# read line history
if isReadlineEnabled:
	try:
	    readline.read_history_file(Settings.HISTORY_FILE)
	except IOError:
	    pass

# process input from the user
isSocketUp = True
while running:
    try:
        # retrieve the input from the user
        input = raw_input("proxy> ")
        cmd = input.split(' ')

        # execute command
        if re.search('^quit$',cmd[0].lower()):
            eventloop.postMessage((Settings.ADM_QUIT_PROGRAM,0))
            running = False

        if re.search('^down$',cmd[0].lower()) and memory['isConnected']:
            eventloop.postMessage((Settings.ADM_DISCONNECT,0))
            memory['isConnected'] = False

        if re.search('^up$',cmd[0].lower()) and not memory['isConnected']:
            inlayer.createServer()
            in_select.append(inlayer.getSocket())
            objects[inlayer.getSocket()] = inlayer
            memory['isConnected'] = True

        if re.search('^status$',cmd[0].lower()):
            print memory

        if re.search('^set$',cmd[0].lower()):
            try:
                memory[cmd[1].lower()] = eval(cmd[2],{},{})
            except:
                pass

    except KeyboardInterrupt:
        eventloop.postMessage((Settings.ADM_QUIT_PROGRAM,0))

# wait for threads termination
eventThread.join()

# write the history
if isReadlineEnabled:
	readline.write_history_file(Settings.HISTORY_FILE)

print 'Proxy stopped'

